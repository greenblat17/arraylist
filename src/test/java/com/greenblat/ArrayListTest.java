package com.greenblat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayListTest {
    private List<Integer> list;


    @BeforeEach
    void addElementsToList() {
        list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
    }


    @Test
    void createArrayWithInvalidCapacity() {
        assertThrows(IllegalArgumentException.class, () -> list=  new ArrayList<>(-5));
    }

    @Test
    void checkSizeList() {
        assertEquals(5, list.size());
    }

    @Test
    void checkEmptyList() {
        list = new ArrayList<>();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
    }


    @Test
    void addElements() {
        list.add(5);
        assertEquals(6, list.size());
        assertEquals(5, list.get(5));
        assertTrue(list.contains(5));
    }


    @Test
    void addHeadByIndex() {
        list.add(-1, 0);
        assertEquals(-1, list.get(0));
        assertEquals(0, list.get(1));
        assertEquals(6, list.size());
    }

    @Test
    void addMiddleByIndex() {
        list.add(100, 2);
        assertEquals(100, list.get(2));
        assertEquals(2, list.get(3));
        assertEquals(6, list.size());
    }

    @Test
    void addEndByIndex() {
        list.add(5, list.size());
        assertEquals(5, list.get(list.size() - 1));
        assertEquals(6, list.size());
    }

    @Test
    void addByIndexInEmptyList() {
        list = new ArrayList<>();
        list.add(1, 0);
        assertEquals(1, list.get(0));
        assertEquals(1, list.size());
    }

    @Test
    void addByInvalidIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(100, -1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(100, list.size() + 1));
    }

    @Test
    void addElementsGreaterDefaultCapacityList() {
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        assertEquals(0, list.get(5));
        assertEquals(15, list.size());
    }

    @Test
    void getHeadElement() {
        assertEquals(0, list.get(0));
    }

    @Test
    void getMiddleElement() {
        assertEquals(2, list.get(2));
    }

    @Test
    void getElementByInvalidIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(list.size()));
    }

    @Test
    void getFromEmptyList() {
        list = new ArrayList<>();
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
    }

    @Test
    void removeHeadByIndex() {
        assertTrue(list.remove(0));
        assertEquals(1, list.get(0));
        assertEquals(4, list.size());
    }

    @Test
    void removeMiddleElementByIndex() {
        assertTrue(list.remove(2));
        assertEquals(3, list.get(2));
        assertEquals(4, list.size());
    }

    @Test
    void removeEndElementByIndex() {
        assertTrue(list.remove(list.size() - 1));
        assertEquals(3, list.get(list.size() - 1));
        assertEquals(4, list.size());
    }

    @Test
    void removeHeadByObject() {
        assertTrue(list.remove(Integer.valueOf(0)));
        assertEquals(1, list.get(0));
        assertEquals(4, list.size());
    }

    @Test
    void removeMiddleElementByObject() {
        assertTrue(list.remove(Integer.valueOf(2)));
        assertEquals(3, list.get(2));
        assertEquals(4, list.size());
    }

    @Test
    void removeEndElementByObject() {
        assertTrue(list.remove(Integer.valueOf(list.size() - 1)));
        assertEquals(3, list.get(list.size() - 1));
        assertEquals(4, list.size());
    }

    @Test
    void removeByObjectFromEmptyList() {
        list = new ArrayList<>();
        assertFalse(list.remove(Integer.valueOf(0)));
        assertEquals(0, list.size());
    }

    @Test
    void removeByIndexFromEmptyList() {
        list = new ArrayList<>();
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(0));
        assertEquals(0, list.size());
    }

    @Test
    void removeByInvalidIndex() {
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(list.size()));
    }

    @Test
    void listContainsHeadElement() {
        assertTrue(list.contains(0));
    }

    @Test
    void listContainsMiddleElement() {
        assertTrue(list.contains(2));
    }

    @Test
    void listContainsEndElement() {
        assertTrue(list.contains(4));
    }

    @Test
    void listNotContainsElement() {
        assertFalse(list.contains(list.size()));
    }

    @Test
    void clearList() {
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    void clearEmptyList() {
        list.clear();
        assertEquals(0, list.size());
    }
}