package com.greenblat;

public interface List<E> {
    int size();

    boolean isEmpty();

    void add(E e);

    void add(E e, int index);

    boolean remove(Object o);

    boolean remove(int index);

    E get(int index);

    boolean contains(Object o);

    void clear();


}
