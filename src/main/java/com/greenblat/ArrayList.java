package com.greenblat;



public class ArrayList<E> implements List<E>{

    private final int DEFAULT_CAPACITY = 10;

    private Object[] values;
    private int size = 0;

    public ArrayList(int capacity) {
        if (capacity < 0)
            throw new IllegalArgumentException();
        values = new Object[capacity];
    }

    public ArrayList() {
        values = new Object[DEFAULT_CAPACITY];
    }

    private void ensureCapacity(int requiredLength) {
        if (values.length < requiredLength) {
            int size = (int) (values.length * 1.5);
            values = (Object[]) Arrays.copyOf(values, size);
        }
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public void add(E e) {
        ensureCapacity(size + 1);
        values[size++] = e;
    }

    @Override
    public void add(E e, int index) {
        if (index < 0 || index > size)
            throw new IndexOutOfBoundsException(index);

        ensureCapacity(size + 1);
        Arrays.arraycopy(values, index, values, index + 1, size - index);
        values[index] = e;

        size++;
    }

    @Override
    public boolean remove(Object o) {
        boolean isReadyToRemove = false;
        for (int i = 0; i < size; i++) {
            if (values[i].equals(o))
                isReadyToRemove = true;

            if (isReadyToRemove) {
                if (i == size - 1)
                    values[i] = null;
                else
                    values[i] = values[i + 1];
            }

        }

        if (isReadyToRemove) {
            --size;
        }
        return isReadyToRemove;
    }

    @Override
    public boolean remove(int index) {
        checkIndex(index);

        Object result = values[index];
        Arrays.arraycopy(values, index + 1, values, index, size - 1 - index);

        size--;
        return true;
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return (E) values[index];
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (values[i].equals(o))
                return true;
        }
        return false;
    }

    @Override
    public void clear() {
        values =  new Object[DEFAULT_CAPACITY];
        size = 0;
    }


}
