package com.greenblat;

public class Arrays {

    public static void arraycopy(Object[] src, int srcPos, Object[] dest, int destPos, int length) {
        if (destPos <= srcPos) {
            for (int i = 0; i < length; i++) {
                dest[destPos++] = src[srcPos++];
            }
        } else {
            destPos = destPos + length - 1;
            srcPos = srcPos + length - 1;
            for (int i = 0; i < length; i++) {
                dest[destPos--] = src[srcPos--];
            }
        }
    }

    public static Object copyOf(Object[] original, int newLength) {
        Object[] arrayToCopy = new Object[newLength];
        arraycopy(original, 0, arrayToCopy, 0, original.length);
        return arrayToCopy;
    }
}
